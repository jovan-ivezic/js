'use strict';

// function pozdrav() {
//     console.log('Zdravo');
// }

// pozdrav.jezik = 'srpski';

// // console.log(pozdrav.jezik);

// greet();

// function greet() {
//     console.log('hi');
// }


// var anonimniPozdrav = function() {
//     console.log('hi');
// }
// anonimniPozdrav();

// function log(a) {
//     console.log(a);
// }

// log(3);

// log('hello');

// log({ greeting: 'hi'});

// log(function() {
//     console.log('hi');
// });

// function greet(whatToSay) {

//     return function(name) {
//         return whatToSay + " " + name;
//     }
// }

// var sayHiTo = greet('Hello');
// ovde je vrednost fuunkcije
// sayHiTo('Mirko');

// const myModule = (function () {
//     let secretKey = 9876;

//     function init() {
//         return secretKey;
//     }
    
//     return {
//         init: init
//     }
// })();

// console.log(myModule.init());

let dog = {
    sound       : "vauuuuuu",
    dogSound    : function(){
        console.log(this.sound)
    } 
}

let button = document.getElementById('js_action_btn')



// kada se klikne na dugme pozivamo metodu dogSound koja kaze 
// this.sound ali this sound na dugmetu nije vauuuuu vec je 
// THIS u objektu WINDOWS prozora , zato dodajemo BIND(DOG) 
// da bi promenuli kontekst THIS na dog funkciju
button.addEventListener(
    'click',
    dog.dogSound.bind(dog)
)






