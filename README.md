JS tutorial

Content

1. Variables

    a) Let, const, var

2. Tipovi podataka (int, string, bool, array ...)

3. Arrays

4. Array properties and methods

5. For loop

6. For-each loop

7. While loop

8. Functions

9. Self Invoked functions

10. Objects

11. Predefined objects

12. Constructor functions

13. Scope

   a) This 
   b) Call, apply, bind
   
14. Closures

15. Callback

16. prototype

17. Inheritance Classical VS Prototype 

18. Reflection and Extend

19. Promise

20. By Value VS by reference

21. ES6 and classes