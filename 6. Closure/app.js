// 'use strict';

//This
function foo() {
    console.log(this.bar);
}

var bar = 'global';

var obj1 = {
    bar: 'obj1',
    foo: foo
};

var obj2 = {
    bar: 'obj2'
}

foo(); //global
obj1.foo(); //obj1
foo.call( obj2 ); //obj2
new foo();

const person = {
    firstName: 'jovan',
    lastName: 'ivezic'
}

function getFullName() {
    return "Puno ime i prezime je : " + this.firstName + " " + this.lastName;
}

// person.call(getFullName());

//Call example
var obj = {num: 2}

var addToThis = function(a) {
    return this.num + a;
}

console.log(addToThis.call(obj, 3));
//functionname.call(obj, functionargumets);

var arr = [1, 2, 3];

//Closure
function User() {
    var username, password;

    function doLogin(usr,psw) {
        username = usr;
        password = psw;
    }

    var publicApi = {
        login: doLogin
    }

    return publicApi;
}

//create user module instance
var fred = User();

fred.login('fred', 'fred1234');
