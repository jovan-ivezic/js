'use strict';
//Some useful array methods

//1.Converting from string to array with split()
var myData = 'Manchester,London,Liverpool,Birmingham,Leeds,Carlisle';
var myArray = myData.split(',');
myArray;

//2. Converting array to string join();
var myNewString = myArray.join(', ');

//or use toString() method
var dogNames = ['Rocket','Flash','Bella','Slugger'];
dogNames.toString();

//Adding and removing array items at the end. Use push() and pop()

dogNames.push('Aro'); //Add item to the end
dogNames.push('John', 'Black'); //Add item to the end
dogNames.pop(); // delete last item

//save removed item to variable

var removedItem = myArray.pop();
myArray;
removedItem;

//Adding the and removing array items from start. Use unshift() and shift()
myArray.unshift('Edinburgh');

var removedItem = myArray.shift();//save deleted item
myArray;
removedItem;



//Practice 1
var list = document.querySelector('.output-list');
var totalBox = document.querySelector('.output-total');
var total = 0;
// list.innerHTML = '';
// totalBox.textContent = '';

var products = ['Underpants:6.99', 'Socks:5.99', 'T-shirt:14.99', 'Trousers:31.99', 'Shoes:23.99'];

for (var i = 0; i < products.length; i++) { 
    var newProductsArray = products[i].split(':');
    var name = newProductsArray[0];
    var price = Number(newProductsArray[1]);

    total += price;
  
    // number 5
    var itemText = '';
    
    itemText = name + " - $" + price;

    var listItem = document.createElement('li');
    listItem.textContent = itemText;
    list.appendChild(listItem);
}

totalBox.textContent = 'Total: $' + total.toFixed(2);

//Practice 2
var list1 = document.querySelector('.output-list1');
var searchInput = document.querySelector('.input1');
var searchBtn = document.querySelector('.button1');

list1.innerHTML = '';

var myHistory = [];

searchBtn.onclick = function() {
  // we will only allow a term to be entered if the search input isn't empty
  if (searchInput.value !== '') {
    // number 1
    myHistory.unshift(searchInput.value);

    // empty the list so that we don't display duplicate entries
    // the display is regenerated every time a search term is entered.
    list1.innerHTML = '';

    // loop through the array, and display all the search terms in the list
    for (var i = 0; i < myHistory.length; i++) {
      itemText = myHistory[i];
      var listItem = document.createElement('li');
      listItem.textContent = itemText;
      list1.appendChild(listItem);
    }

    // If the array length is 5 or more, remove the oldest search term
    if (myHistory.length >= 5) {
        myHistory.pop();
    }

    // empty the search input and focus it, ready for the next term to be entered
    searchInput.value = '';
    searchInput.focus();
  }
}


//Practice 3 - Number guesing game

let randomNumber = Math.floor(Math.random() * 100) + 1;

const guesses = document.querySelector('.guesses');
const lastResult = document.querySelector('.lastResult');
const lowOrHi = document.querySelector('.lowOrHi');

const guessSubmit = document.querySelector('.guessSubmit');
const guessField = document.querySelector('.guessField');

let guessCount = 1;
let resetButton;

function checkGuess() {
    let userGuess = Number(guessField.value);
    if (guessCount === 1) {
        guesses.textContent = 'Prethodni pokusaji: ';
    }

    guesses.textContent += userGuess + ' ';

    if (userGuess === randomNumber) {
        lastResult.textContent = 'Cestitamo! Pogodili ste! ' + 'Trazeni broj je: ' + userGuess;
        lastResult.style.backgroundColor = 'green';
        lowOrHi.textContent = '';
        setGameOver();
    } else if (guessCount === 10) {
        lastResult.textContent = '!IGRA JE GOTOVA!';
        lowOrHi.textContent = '';
        setGameOver();
    } else {
        lastResult.textContent = 'Pogresno!';
        lastResult.style.backgroundColor = 'red';
        if (userGuess < randomNumber) {
            lowOrHi.textContent = 'Zadnji pogodak je suvise nizak';
        } else if (userGuess > randomNumber) {
            lowOrHi.textContent = 'Zadnji pogodak je previsok';
        }
    }

    guessCount ++;
    guessField.value = '';
    guessField.focus();
}

guessSubmit.addEventListener('click', checkGuess);

function setGameOver() {
    guessField.disabled = true;
    guessSubmit.disabled = true;
    resetButton = document.createElement('button');
    resetButton.textContent = 'Zapocni novu igru';
    document.querySelector('.resultParas').appendChild(resetButton);
    resetButton.addEventListener('click', resetGame);
}

function resetGame() {
    guessCount = 1;

    const resetParas = document.querySelectorAll('.resultParas p');

    for(let i = 0; i < resetParas.length; i++) {
        resetParas[i].textContent = '';
    }

    resetButton.parentNode.removeChild(resetButton);
    guessField.disabled = false;
    guessSubmit.disabled = false;
    guessField.value = '';
    guessField.focus();
    lastResult.style.backgroundColor = 'white';

    randomNumber = Math.floor(Math.random() * 100) + 1;
}

//Get longest array item
function longestItem() {

    var array = ['apple', 'strawberry', 'orange'];
    var itemLength = 0;
    var longest;

    for (var i = 0; i < array.length; i++) {

        if (array[i].length > itemLength) {
            itemLength = array[i].length;
            longest = array[i];
        }
    }
    console.log('The longest array item is: ', longest);

    document.getElementById('output-1').innerHTML = '<p>The longest array item is: <b>' + longest + '</b></p>';
}

longestItem();

//Write string part
function writeStringPart() {
    var someString = 'Apple, Banana, Kiwi';
    var result = someString.slice(7,13);
    console.log(result);
    document.getElementById('output-2').innerHTML = '<p>Used slice function to select string part: <b>' + result +'</b></p>';
}

writeStringPart();

// funkcija koja radi secenje reci iz stringa
function cutString(string, word) {
    let numChar = word.length;
    let indexStart = string.indexOf(word);
    let indexEnd = indexStart + numChar;

    if (indexStart == -1) {
        var result = "We can't find that word in string";
    } else {
        var result = string.slice(indexStart, indexEnd);
    } 
    console.log(result);
}

cutString('Jovan ivezic local host', 'ivezic');


//String transformation
function stringTransformation() {
    var someString = 'Hello World!';
    var result = someString.toUpperCase();
    var result2 = someString.toLowerCase();
    document.getElementById('output-3').innerHTML = '<p>Uppercase words: <b>' + result + '</b><br>Lowercase words: <b>' + result2 + '</b></p>';
}

stringTransformation();

function stringConcat(str1, str2) {
    var result = str1 +'+'+ str2;
    document.getElementById('output-4').innerHTML = '<p>Concat two words: <b>' + result + '</b></p>';
}

stringConcat("jovan","ivezic");

function stringConcat2(str1, str2) {
    var result = str1.concat('+', str2);
    document.getElementById('output-4').innerHTML += '<p>Concat two words with concat() function: <b>' + result + '</b></p>';
}

stringConcat2("Nemanja","Nemanjic");

//Map example
var niz = [3, 5, 7, 2, 8, 1, 9, 2, 9, 10, 3, 6, 3, 2, 7, 3, 5, 6, 10, 9, 1, 9]
var novNiz;
novNiz = niz.map(function(el, index, arr){
    return el * 2;
});

//Filter
let parniNiz = niz.filter(function(el){
    return el % 2 === 0;
});


//Spread opperator
let niz1 = [1,2,4,5,6];
let niz2 = [3,4];

let noviNiz = [1,2, ...niz2, 5, 6, 7];

// console.log(noviNiz);

// let samoParniBrojevi = niz1.filter(n => n % 2 === 0 && n > 5 && n < 60 );
// let samoParniBrojevi = niz1.filter(element, => {
//     return element > 5 && element < 60;
// });

// console.log(samoParniBrojevi);

