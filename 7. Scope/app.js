
function a() {
    var myVar = 2;
    b();
}

function b() {
    console.log(myVar);
}

var myVar = 1;

a();

function waitThreeSeconds() {
    var ms = 3000 + new Date().getTime();
    
    while (new Date() < ms) {}
    console.log('Finished function');
}

function clickEvent() {
    console.log('Click event!');
}

document.addEventListener('click', clickEvent);

waitThreeSeconds();

console.log('Finished execution');