/* -------------------------------------
    Inline modal
* ------------------------------------- */
'use strict';

//Open popup based on link id
(function openPopup() {

    var inlineModalLink = document.getElementsByClassName('js_inline_modal');

    for(var i = 0; i < inlineModalLink.length; i++) {
        var trigger = inlineModalLink[i];

        trigger.addEventListener('click', function() {
            var modalId = document.getElementById(this.getAttribute('data-id'));
            modalId.classList.add('is_active');
        });
    }
})();


//One function to close modal (click on close button and click outside)
window.onclick = function(event) {
    var target = event.target;
    if (target.classList.contains('modal')) {
        target.classList.remove('is_active');
    } else if (target.classList.contains('js_close')) {
        target.closest('.js_modal').classList.remove('is_active');
    }
}

// Close modal when click on close button
// (function closePopup() {
//     var modal = document.getElementsByClassName('js_modal');
//     var closeButton = document.querySelectorAll('.js_close');

//     closeButton.forEach(function(element, index) {

//         element.addEventListener('click', function(e) {
//             this.closest('.js_modal').classList.remove('is_active');
//         });
//     });
// })();

// Close modal when click outside
// window.onclick = function(event) {
//     var target = event.target;
//     if (target.classList.contains('modal')) {
//         target.classList.remove('is_active');
//     }
// }


/* -------------------------------------
    Single image modal
* ------------------------------------- */


var imageModalLink = document.querySelectorAll('.js_image_modal_link');
var imageModal = document.getElementById('js_image_modal');

imageModalLink.forEach(function(el) {

    el.addEventListener('click', function(e) {
        let modalImage;
        imageModal.classList.add('is_active');
        let imgSrc = e.target.currentSrc;
        let modalContent = imageModal.querySelector('.modal_content');

        //remove all content from modal
        while (modalContent.firstChild) {
            modalContent.removeChild(modalContent.firstChild);
        }

        modalImage = document.createElement('IMG');
        modalImage.src = imgSrc;
        modalImage.className = 'modal_img';
        modalContent.appendChild(modalImage);
    })
});

