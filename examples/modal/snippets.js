// https://stackoverflow.com/questions/22119673/find-the-closest-ancestor-element-that-has-a-specific-class

//support all browsers
function findAncestor (el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;
}