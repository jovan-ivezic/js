var Runner = function(data){

    document.addEventListener('click', function (event) {

        // js click listener
        var modalListener = data.modalListenerSelector;

        // element for inject content
        var modalContentWrapperHtml =  data.modalContentWrapperHtml;

        // modal wrapper
        var modalContentWrapperSelector =  data.modalContentWrapperSelector;
        var modalContentEl = document.getElementById(data.modalContentWrapperHtml);

        // class for open modal
        var openModalSelector = data.openModalSelector;

        // check if element with modal selector exist
        if (document.getElementsByClassName(openModalSelector).length > 0 ){

            var modal = document.getElementById(modalContentWrapperSelector);
            modal.classList.remove(openModalSelector);
            modalContentEl.innerHTML = '';
        }
        if ((event.target).classList.contains(modalListener)) {
            var modal = document.getElementById(modalContentWrapperSelector);
            modal.classList.toggle(openModalSelector);
            clickedElData = event.target.dataset.ref;

            if (clickedElData == undefined) {
                var innerContent = event.target.innerHTML;
            } else {
                var innerContent = data.html;
            }
                modalContentEl.innerHTML = innerContent;

        }

    }, false);

};

return Runner;

new Modal({
    modalListenerSelector: "js-g4-modal",
    modalContentWrapperSelector: "js_modal",
    modalContentWrapperHtml: "js_content",
    timeout: 5,
    type: "gallery",
    html: "<h2>Ovo je naslov i neki text koji se ubacuje</h2>",
    openModalSelector : "is_open"
});