'use strict';

const buttonPressMe = document.querySelector('.button-press-me');

buttonPressMe.onclick = function() {
    let name = prompt("Recite mi vase ime?");
    alert('Hello ' + name + "! Nice to see you!")
}

//Hoisting (this will work)
myName = 'Chris';

function logName() {
  console.log(myName);
}

logName();

var myName;

//Comparation operands
let btnStart = document.querySelector('.start');
let textStop = document.querySelector('.stop');

btnStart.addEventListener('click', updateBtn);

function updateBtn() {

    if (btnStart.textContent === 'Start machine') {
        btnStart.textContent = 'Stop machine';
        textStop.textContent = "The machine is started";
    } else {
        btnStart.textContent = 'Start machine';
        textStop.textContent = 'The machine is stopped.'
    }
}