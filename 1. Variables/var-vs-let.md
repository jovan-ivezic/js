## Difference between using “let” and “var”

(https://stackoverflow.com/questions/762011/whats-the-difference-between-using-let-and-var-to-declare-a-variable-in-jav)

The difference is scoping. var is scoped to the nearest function block and let is scoped to the nearest enclosing block, which can be smaller than a function block. Both are global if outside any block.

Also, variables declared with let are not accessible before they are declared in their enclosing block.

Global:
They are very similar when used like this outside a function block.

```
let me = 'go';  // globally scoped
var i = 'able'; // globally scoped
```

However, global variables defined with let will not be added as properties on the global window object like those defined with var.

```
console.log(window.me); // undefined
console.log(window.i); // 'able'
```

Function:
They are identical when used like this in a function block.

```
function ingWithinEstablishedParameters() {
    let terOfRecommendation = 'awesome worker!'; //function block scoped
    var sityCheerleading = 'go!'; //function block scoped
}
```

Block:
Here is the difference. let is only visible in the for() loop and var is visible to the whole function.

```
function allyIlliterate() {
    //tuce is *not* visible out here

    for( let tuce = 0; tuce < 5; tuce++ ) {
        //tuce is only visible in here (and in the for() parentheses)
        //and there is a separate tuce variable for each iteration of the loop
    }

    //tuce is *not* visible out here
}
```

```
function byE40() {
    //nish *is* visible out here

    for( var nish = 0; nish < 5; nish++ ) {
        //nish is visible to the whole function
    }

    //nish *is* visible out here
}
```

Redeclaration:
Assuming strict mode, var will let you re-declare the same variable in the same scope. On the other hand, let will not:

```
'use strict';
let me = 'foo';
let me = 'bar'; // SyntaxError: Identifier 'me' has already been declared
```

```
'use strict';
var me = 'foo';
var me = 'bar'; // No problem,  'me' is replaced.
```

Anoter example

let can also be used to avoid problems with closures. It binds fresh value rather than keeping an old reference as shown in examples below.

DEMO - (http://jsfiddle.net/rmXcF/5/)

```
for(var i = 1; i < 6; i++) {
  document.getElementById('my-element' + i)
    .addEventListener('click', function() { alert(i) })
}
```

Code above demonstrates a classic JavaScript closure problem. Reference to the i variable is being stored in the click handler closure, rather than the actual value of i.

Every single click handler will refer to the same object because there’s only one counter object which holds 6 so you get six on each click.

General workaround is to wrap this in an anonymous function and pass i as argument. Such issues can also be avoided now by using let instead var as shown in code below.

DEMO (http://jsfiddle.net/rmXcF/4/)

```
'use strict';

for(let i = 1; i < 6; i++) {
  document.getElementById('my-element' + i)
    .addEventListener('click', function() { alert(i) })
}
```

## Examples from Nemanja”

```
var x = 1;

if(x === 1) {
    let x = 2;
    console.log(x);
}
// x is 2 in block and 1 outside of block

var x = 1;

if(x === 1) {
    var x = 3;
    console.log(x);
}
// x is 3 in block and 3 outside of block
```