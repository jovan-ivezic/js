// 'use strict';

//This, call, apply, bind primeri sa casa
function foo() {
    console.log(this.bar);
}

var bar = 'global';

var obj1 = {
    bar: 'obj1',
    foo: foo
};

var obj2 = {
    bar: 'obj2'
}

foo(); //global
obj1.foo(); //obj1
foo.call( obj2 ); //obj2
new foo();

const person = {
    firstName: 'jovan',
    lastName: 'ivezic'
}

function getFullName() {
    return "Puno ime i prezime je : " + this.firstName + " " + this.lastName;
}

// person.call(getFullName());

//Call example
var obj = {num: 2}

var addToThis = function(a) {
    return this.num + a;
}

console.log(addToThis.call(obj, 3));
//functionname.call(obj, functionargumets);

var arr = [1, 2, 3];

//Samostalna edukacija primeri

/*

    This, call, apply, bind

    1. Implicit binding
    2. Explicit binding
    3. new Binding
    4. Wind Binding

    //Where is this function invoked?
*/

var jovan = {
    name: 'Jovan',
    age: 34,
    sayName: function() {
        console.log(this.name);
    }
};

jovan.sayName();

var sayNameMixin = function(obj) {
    obj.sayName = function() {
        console.log(this.name);
    }
};

var me = {
    name: 'Jovan',
    age: 44
};

var you = {
    name: 'Marko',
    age: '22'
};

sayNameMixin(me);
sayNameMixin(you);


//Implicit Binding - implicitno gledamo sta stoji levo od imena funkcije da vidimo sta je this
var Person = function(name, age) {
    return {
        name: name,
        age: age,
        sayName: function() {
            console.log(this.name);
        },
        mother:{
           name: 'Stacey',
           sayName: function() {
                console.log(this.name);
           } 
        } 
    }
};

var jim = Person('Jim', 42);
jim.sayName();//levo je objekat jim i to je this
jim.mother.sayName(); // prvi levo je objekat mother i on je ovde this

//Explicit Binding
//Call apply, bind


//Sa call eksplicitno definisem sta je this a takodje mozemo da prosledimo i druge parametre
var sayName = function(lang1, lang2, lang3) {
    console.log('My name is ' + this.name + ' and I know ' + lang1 + ', ' + lang2 + ', and ' + lang3);
};

var stacey = {
    name: 'Stacey',
    age: 22
};

var languages = ['JavaScript', 'Ruby', 'Python'];

//Call automatski poziva funkciju
sayName.call(stacey, languages[0], languages[1], languages[2]);

//Apply
sayName.apply(stacey, languages); 
// apply radi istu stvar kao call samo sto ne moraju svi argumenti zasebno da se pozovu
// vec moze da se prosledi cela varijbla u ovom slucaju niz

//Bind
var newFn = sayName.bind(stacey, languages[0], languages[1], languages[2]);
//bind radi isto sto i call samo sto vraca novu funkciju umesto da poziva originalnu funkciju
//novu funkciju kasnije mozemo koristiti
newFn();

//new Binding

//Velikim slovima pisemo ime varijable jer ce biti konstruktor funkcija i pozvace se sa new
var Animal = function(color, name, type) {
    //this = {} //this je ovde novi objekat koji nastaje pomocu new
    this.color = color; 
    this.name = name;  
    this.type = type
};

var zebra = new Animal('black and white', 'Zorro', 'Zebra');

// window Binding
var sayAge = function() {
    //sa 'use strict' ovo bi izbacilo gresku
    console.log(this.age);
};

var ja = {
    age: 33
};

sayAge(); 
/* this ove funkcije cilja globalni objekat 
   a posto window nema taj property rezultat je undefined
*/

window.age = 44;

sayAge();

//this
/*
The this keyword’s value has nothing to do with the function itself, 
how the function is called determines this's value
*/

//Default "this" context
var myFunction = function() {
    // 'use strict'; //with use strict this will be undefined here
    console.log(this); //points to global window
};
//myFunction();

//Object literals

// var myObject = {
//     myMethod: function() {
//         console.log(this); //ne znamo sta je this ovde zavisi kako ce funkcija biti pozvana
//     }
// };
//Let's change code

var myMethod = function() {
    console.log(this.a);
}

var myObject = {
    myMethod: myMethod
};

myObject.myMethod(); // this === myObject
// myMethod(); // this === window
// myMethod.call(myObject, args1, args2) //this === myObject
// myMethod.apply(myObject, [array of args]) //this === myObject


var obj1 = {
    a: 2,
    myMethod: myMethod
};

var obj2 = {
    a: 3,
    myMethod: myMethod
};

obj1.myMethod();
obj2.myMethod();

obj1.myMethod.call( obj2 );
obj2.myMethod.call( obj1 );

//Bind

//primer 1
var modul = {
    x: 45,
    getX: function() {
        return this.x;
    }
};

var unboundGetX = modul.getX;
console.log('funcija je na globalu pozvana i ne moze da prisutpi. Rezultat: ', unboundGetX());

var boundGetX = unboundGetX.bind(modul);
console.log(boundGetX());

//primer 2

var osoba = {
    ime: 'Jovan',
    prezime: 'Ivezic',
    ispisiPunoIme: function() {

        var punoIme = this.ime + ' ' + this.prezime;
        return punoIme;

    }
};

var logIme = function(jezik1, jezik2) {
    console.log('Logovan', this.ispisiPunoIme());
    console.log('....................');
    console.log('Arguments:', jezik1 + ', ' + jezik2);
};

var logImeOsobe = logIme.bind(osoba);

logImeOsobe('ruski', 'engleski');

//function barowing
var osoba2 = {
    ime: 'Janko',
    prezime: 'Jankovic'
};

console.log(osoba.ispisiPunoIme.call(osoba2));

//function currying
/*
    Function curring: kreiranje kopije funkcije ali sa predefinisanim setom parametara.
    Veoma korisno kod matematickih funkcijav 
*/
function multiply(a, b) {
    return a * b;
}

var multipleByTwo = multiply.bind(this, 2); //2 je podrazumevani parametar
console.log('Pomnozi sa 2:', multipleByTwo(3123)); //funkcija prima jos jedan dodatni parametar, ako dodamo vise nista se nece dogoditi


var multiplyByThree = multiply.bind(this, 3);
console.log('Pomnozi sa 3: ', multiplyByThree(3004));


//Nemanja primer

let dog = {
    sound       : "vauuuuuu",
    dogSound    : function(){
        console.log(this.sound)
    }
}

let button = document.getElementById('js_action_btn')

// kada se klikne na dugme pozivamo metodu dogSound koja kaze
// this.sound ali this sound na dugmetu nije vauuuuu vec je
// THIS u objektu WINDOWS prozora , zato dodajemo BIND(DOG)
// da bi promenuli kontekst THIS na dog funkciju
button.addEventListener('click',dog.dogSound.bind(dog));

