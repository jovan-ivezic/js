'use strict';
// https://gist.github.com/NenadPavlov/c382b284c7a4c1430334
/*
  U nastavku su test podaci kao i potpisi funkcija. U komentarima je objašnjenje šta koja funkcija treba da uradi i šta treba da vrati kao rezultat izvršenja.
*/

const test_objekat1 = {
    firstName: "Jovan",
    lastName: "Ivezic",
    grade: [1, 6, 2, 5, 7, 2, 8, 3]
};
    
const test_objekat2 = {
    firstName: "Nemanja",
    lastName: "Veljkovic",
    grade: [45, 62, 123, 63, 74, 85, 21, 77]
};

const test_objekat3 = {
    firstName: "Nemanja",
    lastName: "Mirkov",
    grade: [-6, 23, 10, 0, -22, 52, 86, 2, 11]
};

function fullname(person) {
    console.log("Puno ime i prezime je : " + person.firstName + " " + person.lastName);
}

// fullname(test_objekat3);


// Najmanja ocena osobe, funkcija vraća broj
function getMinGrade(person) {
    const objectName = document.querySelector('.object-name-1');
    let objectMinGrade = document.querySelector('.result-1');

    let gradeArray = person.grade;
    let result = gradeArray[0];

    //with for loop
    // for(let i = 0; i < gradeArray.length; i++) {
    //     if (gradeArray[i] < result) {
    //         result = gradeArray[i];
    //     }
    // }

    //with for each loop
    gradeArray.forEach(function(item) {

        if (item <= result) {
            result = item;
        }

    });

    objectName.textContent = person.firstName + ' ' + person.lastName;
    objectMinGrade.textContent = result;

    return result;
}

getMinGrade(test_objekat1);


function getArrayOfEvenGrades(person) {
    const objArray = document.querySelector('.object-name-2');
    const newObjArray = document.querySelector('.result-2');
    let gradeArray = person.grade;
    let newGradeArrayOdd = [];

    gradeArray.forEach(function(item) {

        if (item % 2 === 0) {
            newGradeArrayOdd.push(item);
        }
    });

    objArray.textContent = person.firstName + ' ' + person.lastName + ' grades array: ' + gradeArray;
    newObjArray.textContent = newGradeArrayOdd;

    return newGradeArrayOdd;
}

getArrayOfEvenGrades(test_objekat1);

// Get sum of all grades, function returns number
function getSumOfGrades(person) {
    const objArray = document.querySelector('.object-name-3');
    const gradesResult = document.querySelector('.result-3');

    let gradeArray = person.grade;
    let sumOfGrades = 0;

    gradeArray.forEach(function(item) {
        sumOfGrades += item;
    });

    objArray.textContent = gradeArray;
    gradesResult.textContent = sumOfGrades;

    return sumOfGrades;
}

getSumOfGrades(test_objekat3);


// Novi niz koji se dobija množenjem parnih ocena sa 3, a neparnih sa 4, funkcija vraća niz
function getMappedGrades(person) {
    const objectStartingArray = document.querySelector('.object-name-4');
    const newArrayResult = document.querySelector('.result-4');

    let gradeArray = person.grade;
    let newGradeArrayMapped = [];

    gradeArray.forEach(function(item) {
        if (item %2 === 0) {
            newGradeArrayMapped.push(item * 3);
        } else {
            newGradeArrayMapped.push(item * 4);
        }
    });
    
    objectStartingArray.textContent = gradeArray;
    newArrayResult.textContent = newGradeArrayMapped;
}

getMappedGrades(test_objekat1);

/* 
    Novi niz koji se dobija stavljanjem parnih ocena na početak niza, 
    a neparnih na kraj. Mogu se praviti pomoćni nizovi za parne i neparne 
    pa da se na kraju spoje. Funkcija vraća niz
*/

function reorderGrades(person) {
    const objectArrayStart = document.querySelector('.object-name-5');
    const objectArrayResult = document.querySelector('.result-5');
    
    let gradeArray = person.grade;
    let newGradeArray = [];
    let gradeArrayEven = [];
    let gradeArrryOdd = []

    gradeArray.forEach(function(item) {

        //Solution 1
        if (item %2 === 0) {
            newGradeArray.unshift(item);
        } else {
            newGradeArray.push(item);
        }

        //Solution 2
        // if (item %2 === 0) {
        //     gradeArrayEven.push(item);
        // } else {
        //     gradeArrryOdd.push(item)
        // }
    });

    // newGradeArray = gradeArrayEven.concat(gradeArrryOdd);

    objectArrayStart.textContent = gradeArray;
    objectArrayResult.textContent = newGradeArray;    
}

reorderGrades(test_objekat1);


// Koliko je ocena veće od zadatog broja? Funkcija vraća broj.
function getNumberOfGradesGreaterThan(person, limit) {
    const objectArrayStart = document.querySelector('.object-name-6');
    const limitNumber = document.querySelector('.limit');
    const resultNumber = document.querySelector('.result-6');

    let gradeArray = person.grade;
    let counter = 0;

    gradeArray.forEach(function(item) {
        if (item > limit) {
            counter ++;
        }
    });

    objectArrayStart.textContent = gradeArray;
    limitNumber.textContent = limit;
    resultNumber.textContent = counter;
}

getNumberOfGradesGreaterThan(test_objekat1, 5);

/* 
    Ako je treći argument true treba napraviti novi niz od ocena osobe
    koji su VEĆE ili JEDNAKE od zadatog broja. Ako je treći argument false 
    treba napraviti novi niz od ocena osobe koji su MANJE od zadatog broja. 
    Funkcija vraća niz
*/

function filterOutGrades(person, limit, direction) {
    const objecArrayStart = document.querySelector('.object-name-7');
    const resultArray = document.querySelector('.result-7');

    let gradesArray = person.grade;
    let gradesGreaterThanLimit = [];
    let gradesSmallerThanLimit = [];

    gradesArray.forEach(function(item) {

        if (direction === true && item >= limit) {
            gradesGreaterThanLimit.push(item);
        } 

        if (direction === false && item <= limit) {
            gradesSmallerThanLimit.push(item);
        }
    });

    objecArrayStart.textContent = gradesArray;

    if (direction) {
        resultArray.textContent = 'Grades greather or equal than ' + limit + ' are: ' + gradesGreaterThanLimit;
    } else {
        resultArray.textContent = 'Grades smaller or egual than ' + limit + ' are: ' + gradesSmallerThanLimit;
    }

}

filterOutGrades(test_objekat3, 5, true);

/*
  Dobili smo sirove podatke i zaduženje da izvučemo određenu statistiku. 
  Naš zadatak: Izračunati koliko godina iskustva imaju svi ljudi ukupno.
  
*/
const people = [
  {
    name: 'Joe Schmoe',
    yearsExperience: 1,
    department: 'IT'
  },
  {
    name: 'Sally Sallerson',
    yearsExperience: 15,
    department: 'Engineering'
  },
  {
    name: 'Bill Billson',
    yearsExperience: 5,
    department: 'Engineering'
  },
  {
    name: 'Jane Janet',
    yearsExperience: 11,
    department: 'Management'
  },
  {
    name: 'Bob Hope',
    yearsExperience: 9,
    department: 'IT'
  }
];

function getTotalExperience(results) {

    let total = 0;
    let experienceYears = [];

    results.forEach(function(item) {
        experienceYears = item.yearsExperience;
        total += experienceYears;
    });

    return 'All people have ' + total + ' years of experience';
}

console.log(getTotalExperience(people)); // 41

//solution with reduce
const ljudi = [
  {
    name: 'Joe Schmoe',
    yearsExperience: 1,
    department: 'IT'
  },
  {
    name: 'Sally Sallerson',
    yearsExperience: 15,
    department: 'Engineering'
  },
  {
    name: 'Bill Billson',
    yearsExperience: 5,
    department: 'Engineering'
  },
  {
    name: 'Jane Janet',
    yearsExperience: 11,
    department: 'Management'
  },
  {
    name: 'Bob Hope',
    yearsExperience: 9,
    department: 'IT'
  }
];

function getTotalExperience1(data) {
    var godineIskustva = data.reduce(function (zbirGodina, data) {
        zbirGodina += data.yearsExperience;
        return zbirGodina;
    }, 0)
    return godineIskustva;
}
// console.log(getTotalExperience1(ljudi));


/*
  Stigli su nam podaci sa servera. Podaci su u vidu objekta koji čuva informacije o kursu i između ostalog niz ocena polaznika.
  
  Zadatak: Napisati funkciju koja će kao jedini parametar da prima objekat sa rezultatima. Funkcija treba da vrati NOVI OBJEKAT
  čiji će property-ji biti ocene polaznika u formatu:
  
  {
    mark_1 : 3,
    mark_2 : 5,
    mark_3 : 7,
    mark_4 : 2,
    mark_5 : 8,
    ...
  }
  
  
  Rešenje možete praviti pomoću bilo koje petlje ili metode.
*/

const test_objekat = {
  naziv: "Osnovni kurs JavaScripta",
  trajanje_nedelja: 5,
  predavac: {
    ime: "Nenad",
    prezime: "Pavlov"
  },
  ocene_polaznika: [3, 5, 7, 2, 8, 1, 9, 2, 9, 10, 3, 6, 3, 2, 7, 3, 5, 6, 10, 9, 1, 9],
  organizacija: "DaFED"
};

function getMarksAsObject(results) {

    let ocene = results.ocene_polaznika;
    const newObj = {};

    ocene.forEach(function(item, index) {
        newObj['mark_' + index] = item;
    });
    
    return newObj;
}

//Example with reduce
// function getMarksAsObject(results) {
//   let grade = results.ocene_polaznika;
//   let createGrades = grade.reduce(function(agg, el, index, arr){
//     agg['mark_' + (index + 1)] = el;
//     return agg;
//   }, {})
 
//   return createGrades;
// }
// console.log(getMarksAsObject(test_objekat));
//https://codeburst.io/various-ways-to-create-javascript-object-9563c6887a47

/*
  Dobili smo sirove podatke i zaduženje da izvučemo određenu statistiku.
  Naš zadatak: Napraviti novi objekat koji će da sadrži zaposlene razvrstane po iskustvu, dakle:
  
  {
    AMATEUR: ["Bill Billson"],
    EXPERT: ["Sally Sallerson", "Jane Janet"],
    NEWBIE: ["Joe Schmoe"],
    PRO: ["Bob Hope", "John Doe"]
  }
  
  Value deo svakog propertija je NIZ imena.
  
  Možete koristiti šta god vam je zgodno, ali je preporuka da se uradi i pomoću reduce().
*/

var peopleList = [
  {
    name: 'Joe Schmoe',
    yearsExperience: 1,
    department: 'IT'
  },
  {
    name: 'Sally Sallerson',
    yearsExperience: 15,
    department: 'Engineering'
  },
  {
    name: 'Bill Billson',
    yearsExperience: 5,
    department: 'Engineering'
  },
  {
    name: 'Jane Janet',
    yearsExperience: 11,
    department: 'Management'
  },
  {
    name: 'Bob Hope',
    yearsExperience: 9,
    department: 'IT'
  },
  {
    name: 'John Doe',
    yearsExperience: 7,
    department: 'Management'
  }
];

//Moje resenje
// function sortEmployeesByExperience(results) {
//   let newObject = {};
//   let newbieArray = [], amateurArray = [], proArray = [], expertArray = [];

//   results.forEach(function(item) {

//     if (item.yearsExperience <= 1) {
//         newbieArray.push(item.name);
//         newObject.NEWBIE = newbieArray;
//     } else if (item.yearsExperience <= 5) {
//         amateurArray.push(item.name);
//         newObject.AMATEUR = amateurArray;
//     } else if (item.yearsExperience <= 10) {
//         proArray.push(item.name);
//         newObject.PRO = proArray;
//     } else {
//         expertArray.push(item.name);
//         newObject.EXPERT = expertArray;
//     }
//   });
//   return newObject;
// }

// console.log(sortEmployeesByExperience(peopleList));


//Pomoćna funkcija za klasifikaciju:
function classifyExperience(employee) {
  var years = employee.yearsExperience;

    if (years <= 1) {
        return 'NEWBIE';
    }  else if (years <= 5) {
        return 'AMATEUR';
    } else if (years <= 10) {
        return 'PRO';
    } else {
        return 'EXPERT';
    }
}

function sortEmployeesByExperiencemoj(results) {
    const iskustvoZaposlenih = {};

    results.forEach(function(item) {

        let level = classifyExperience(item);

        if ( iskustvoZaposlenih[level] === undefined ) {
            iskustvoZaposlenih[level] = [];
        }

        iskustvoZaposlenih[level].push(item.name);
    });
    return iskustvoZaposlenih;
}

//Solution with reduce
// function sortEmployeesByExperience(results) {
//     return results.reduce(function(agg, el) {
//         var category = classifyExperience(el);

//         if(agg[category] === undefined) {
//             agg[category] = [];
//         }

//         agg[category].push(el.name);

//         return agg;

//     }, {});
// }

console.log(sortEmployeesByExperiencemoj(peopleList));



/*
  Dobili smo sirove podatke i zaduženje da izvučemo određenu statistiku.
  Naš zadatak: Napraviti novi objekat koji će da sadrži broj zaposlenih u svakom departmentu, dakle:
  
  {
    Engineering: 2,
    IT: 2,
    Management: 1
  }
  
  Možete koristiti šta god vam je zgodno.
*/

const people10 = [
  {
    name: 'Joe Schmoe',
    yearsExperience: 1,
    department: 'IT'
  },
  {
    name: 'Sally Sallerson',
    yearsExperience: 15,
    department: 'Engineering'
  },
  {
    name: 'Bill Billson',
    yearsExperience: 5,
    department: 'Engineering'
  },
  {
    name: 'Jane Janet',
    yearsExperience: 11,
    department: 'Management'
  },
  {
    name: 'Bob Hope',
    yearsExperience: 9,
    department: 'IT'
  }
];

function getNumberOfEmployeesByDepartments(results){
    const noviObjekat = {};

    results.forEach(function(item) {

        let odeljenje = item.department;

        if (noviObjekat[odeljenje] === undefined) {

            noviObjekat[odeljenje] = 0;
        }

        noviObjekat[odeljenje] += 1;
    });

    return noviObjekat;

}

console.log(getNumberOfEmployeesByDepartments(people10));

// console.log(sortEmployeesByExperience(ljudi));

/*
 Napisati funkciju koja će za ulazne podatke vratiti niz stringova u željenom formatu.
 // https://codepen.io/nenadpavlov/pen/69d5ab60792c078bfaeb935f9a766a9f?editors=0012
*/

var test_data12 = [
  {
    name: "Djura",
    modeli: [{ tip: 14 },{ tip: 2 }, { tip: 9 }]
  },
  {
    name: "Pera",
    modeli: [{ tip: 11 }, { tip: 4 },{ tip: 48 }]
  },
  {
    name: "Mika",
    modeli: [{ tip: 6 },{ tip: 4 },{ tip: 2 }]
  }  
];

// Želimo da dobijemo sledeći output:

/*
["Djura - tip 14", 
"Djura - tip 2", 
"Djura - tip 9", 
"Pera - tip 11", 
"Pera - tip 4", 
"Pera - tip 48", 
"Mika - tip 6", 
"Mika - tip 4", 
"Mika - tip 2"]
*/

// Znači u pitanju je niz, elemente sam poređao vertikalno radi jasnoće, a svaki string se formira: Ime osobe + prazno mesto + crtica + prazno mesto + "task" + prazno mesto + vrednost

function getModels(data) {

    let newArray = [];
    let models = [];
    let names = [];

    data.forEach(function(item) {

        models = item.modeli;
        names = item.name;

        models.forEach(function(iteminner) {
            newArray.push(names + ' - ' + Object.keys(iteminner) + ' ' + iteminner.tip)
        });
    });

    return newArray;
}

// console.log(getModels(test_data12));




// function getModels(data) {
//   const result = [];
//   data.forEach((person) => {
//     person.modeli.forEach((model) => {
//       result.push(`${person.name} - ${Object.keys(model)[0]} ${model.tip}`);
//     });
//   });
//   return result;
// }

// console.log(getModels(test_data));


// function nizOdObjekta(data) {

//     var result = [];

//     data.forEach(function (korisnik) {
//         var imena = korisnik.name;
//         korisnik.modeli.forEach(function (model) {
//             result.push(imena + " - " + Object.keys(model) + " " + model.tip);
//         });

//     })
//     console.log(result);
// }

// nizOdObjekta(test_data12);


/*
https://codepen.io/nenadpavlov/pen/1636c7591a2ed5349c0374b4fd4837e6?editors=0012

Telemarketing kompanija (oni što zovu na fiksni pa nude koješta) nas je unajmila da im pomognemo oko obrade velike količine podataka.

Naš posao je napišemo 3 funkcije za različite svrhe.

1. Treba da vrati novi niz koji će sadržati objekte kao svoje elemente sa podacima ljudi koji ispunjavaju kriterijum da su strariji ili imaju zadati broj godina.

2. Za posebne svrhe treba im mogućnost filtriranja ljudi u određenom starosnom opsegu. Takođe funkcija vraća novi niz čiji su elementi objekti.

3. Na kraju, radi lakšeg pregleda i zvanja, žele da imaju funkciju poput 2. ali da elementi novog niza kojeg funkcija vraća budu stringovi sačinjeni samo od imena, prezimena i broja telefona ljudi koji ispunjavaju kriterijum starosnog opsega.

Poslali su nam test set podataka kako bismo imali uvid u format koji koriste i da bismo mogli da testiramo naše rešenje.

Zadatak rešiti na način po sopstvenoj želji.

*/

var test_data = [
    {
        name: "Buck",
        lastname: "Gilbert",
        age: 38,
        phone: 423912
    },
    {
        name: "Rowan",
        lastname: "Read",
        age: 108,
        phone: 174232
    },
    {
        name: "Darren",
        lastname: "Harlan",
        age: 28,
        phone: 7536435
    },
    {
        name: "Hyram",
        lastname: "Styles",
        age: 15,
        phone: 423912
    },
    {
        name: "Kev",
        lastname: "Levitt",
        age: 18,
        phone: 428999
    },
  {
        name: "Sidney",
        lastname: "Attwood",
        age: 32,
        phone: 788675
    },
  {
        name: "Brenden",
        lastname: "Langdon",
        age: 53,
        phone: 9990099
    }
];

// 1. Isfiltrirati osobe koje su starije ili imaju zadati broj godina 

function getOlderThan(data, age) {

    let newArray = [];
  
    data.forEach(function(item) {

        if (item.age >= age) {
            newArray.push(item);
        }
    });
    return newArray;
}
console.log(getOlderThan(test_data, 18)); // Primer. Uključiti i one koji imaju 18

// + -----------------------------------------------------------+
//  2. Isfiltrirati osobe čije su godine u zadatom rasponu (inclusive - i oni koji imaju jednako godina sa donjim ili gornjim limitom)

function getInBetween(data, start_age, end_age) {
  // ovde logika
  let newArray = [];

  data.forEach(function(item) {
    if (item.age >= start_age && item.age <= end_age) {
        newArray.push(item);
    }
  });
  return newArray;
}
console.log(getInBetween(test_data, 30, 53)); // Primer. Uključiti i one koji imaju 20 i 53

/* + -----------------------------------------------------------+
// 3. Isfiltrirati osobe čije su godine u zadatom rasponu (inclusive, važi isto kao kod 2.) i izvući samo njihovo ime, prezime i broj telefona u vidu jednog stringa, npr:

Za recimo ovaj objekat:
  {
        name: "Brenden",
        lastname: "Langdon",
        age: 53,
        phone: 9990099
    }
  
Želimo da dobijemo string:

"Brenden Langdon - 9990099"

Dakle, šablon za kreiranje stringa je ime + razmak + prezime + crta + broj telefona

*/

function getNamesAndPhone(data, start_age, end_age) {
  let newString = '';

  data.forEach(function(item) {
    if (item.age >= start_age && item.age <= end_age) {
        newString += item.name + ' ' + item.lastname + ' - ' + item.phone + '\n';
    }
  });
  return newString;
}
console.log(getNamesAndPhone(test_data, 30, 53)); // Primer. Uključiti i one koji imaju 20 i 53



